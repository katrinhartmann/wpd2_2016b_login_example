# Simple Message Page

This is a simple example of a web application that allows users to send messages. The message page is password protected.

The code is licensed under the GPL Version 3.

## Requirements

To run it you need [Java 8][2], [Maven][3] and (optionally) [Intellij][4] for which there is a project set up.

# Using Git
    
The repository is stored in Git. This initial version is on the 'master' branch.  
To get started, clone the repository.

# Get started

You must have Java and Maven installed and can run Maven with the <code>mvn</code> command at a prompt.
To check this get up a command prompt and type <code>mvn --version</code> (note two hyphens).  You should
see a version number and other stuff about the Java version. 

By default you are running from the address `http://localhost:9000` on the development server. All the paths
below are relative to this, which may differ for you if you change it.

Open a web browser and go to: `http://localhost:9000/ You will see the index page with a link 'Send a message'. 
Clicking this link takes you to the login page if not already logged in. Otherwise you can go 
start to send messages.    
    
## Technology stack in the starter

The starter uses [Jetty][1] to run an embedded web server.

The main class is `Runner`. This starts an embedded Jetty Server. 

net.katrinhartmann.shop.Runner.


[1]: https://eclipse.org/jetty/
[2]: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
[3]: http://maven.apache.org/download.cgi
[4]: http://www.jetbrains.com/idea/
[5]: https://github.com/google/guice/wiki/GettingStarted
[6]: https://github.com/google/guice/wiki/Servlets