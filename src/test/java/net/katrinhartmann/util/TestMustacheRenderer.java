

package net.katrinhartmann.util;

import lombok.Data;
import org.junit.Before;
import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

public class TestMustacheRenderer {
    @SuppressWarnings("unused")
    static final Logger LOG = LoggerFactory.getLogger(TestMustacheRenderer.class);

    private MustacheRenderer mustache;

    public TestMustacheRenderer() {}

    @Before
    public void setUp() {
        mustache = new MustacheRenderer("net/katrinhartmann/util");
    }

    @Test
    public void testRenderer() {
        String out = mustache.render("testTemplate.mustache", new TestClass());
        assertEquals("one", out);
    }

    @Data
    private static class TestClass {
        private String var = "one";
    }
}